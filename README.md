# Trabalho Pratico da Disciplina IDW - IGTI
.
Esse projeto foi um trabalho pratico da disciplina **IDW - Infraestrutura para Desenvolvimento Web**, consistindo em implementar em um servidor qualquer de entrega contínua a publicação ou construção automática de algum artefato.

Desenvolvido por Luis Carlos de Miranda Santos Junior


## Aplicação Escolhida
Foi utilizado um projeto demo em Grails 3 com base em um tutorial do site [grails.org](https://grails.org) que ensina a testar *domains* e *controllers*. Como o projeto já estava pronto quando implementei CI/CD, não experimentei os beneficios da integração continua.

## Dificuldades
Todos os erros e dificuldade foram referentes a configuração do arquivo “gitlab-ci.yml”.
Tive erros devido a ordem dos “stages”. Tive falhas nos Jobs decorrente da utilização de uma imagem “alpine”, e descobri que elas não tem o *bash, git ou curl*, então alguns scripts utilizados (*gradlew e dpl*) não funcionavam. 

O método apresentado em aula para implementar entrega continua no *Heroku*. Segui então o exemplo para um aplicação em scala disponível na documentação do *Gitlab* em [https://docs.gitlab.com/ee/ci/examples/test-scala-application.html](https://docs.gitlab.com/ee/ci/examples/test-scala-application.html).

```yml
image: java:8

stages:
  - deploy

before_script:
  - apt-get update -y
  - apt-get install apt-transport-https -y
  ## Install SBT
  - echo "deb http://dl.bintray.com/sbt/debian /" | tee -a /etc/apt/sources.list.d/sbt.list
  - apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 642AC823
  - apt-get update -y
  - apt-get install sbt -y
  - sbt sbtVersion

deploy:
  stage: deploy
  script:
    - apt-get update -yq
    - apt-get install rubygems ruby-dev -y
    - gem install dpl
    - dpl --provider=heroku --app=gitlab-play-sample-app --api-key=$HEROKU_API_KEY
```

Removi as partes referentes a ferramenta de build **sbt**, e pra evitar a necessidade de instalar o *ruby, gems e dependencias* que estava deixando o pipeline mais lento utilizei a **imagem ruby:2.4-alpine** apenas para o job heroku deploy. Ficando assim

~~~yml
before_script: 
  - apk update && apk add curl git

stages:
- deploy

Heroku Deploy:
  image: ruby:2.4-alpine
  stage: deploy
  script:
    - gem install dpl
    - dpl --provider=heroku --app=fierce-basin-88223 --api-key=$HEROKU_API_KEY
~~~

Também foi necessário acrescentar um arquivo chamado Procfile ao projeto, que diz como rodar o artefato java, ao *Heroku*. Segundo documentação em [https://devcenter.heroku.com/articles/deploying-gradle-apps-on-heroku](https://devcenter.heroku.com/articles/deploying-gradle-apps-on-heroku).

```
web: cd build ; java -Dgrails.env=prod -jar ../build/server/webapp-runner-*.jar --expand-war --port $PORT libs/*.war
```

Também foi necessário fazer alguns ajuste no arquivo build.gradle da aplicação e remover dependências desnecessárias, acresentar o *webapp-runner* e a opção `--expand-war`, o que diminuiu o consumo de ram da aplicação de cerca de 900mb para 300mb, permitindo que a aplicação rodasse nos dynos do *Heroku* que possuem apenas 512mb de ram.
***
###Importante
Quando a aplicação consome mais do que os 512mb disponíveis no dyno do *Heroku*. A aplicação não roda, mas o pipeline acusa o *Deploy* como bem sucedido e só é possível verificar o erro nos logs do *Heroku*.

Não encontrei um meio de falhar o deploy quando a aplicação deixa de rodar por falta de memória.
***


## Configurações
Criei um pipeline com 3 stages: *Test, Build e Deploy*.

O *Test* tem 2 jobs, um faz checkagem estática do código em groovy e outro executa os testes e gera os relatórios para serem utilizados no “gitlab pages”.

O *Build* gera os artefatos “.war” referentes ao projeto e disponibiliza os testes no [Gitlab Pages](https://lmiranda.gitlab.io/trabalhopratico/)

O *Deploy* faz o deploy do projeto para o heroku.

## Links
**Gilab sources:** [https://gitlab.com/lmiranda/trabalhopratico.git](https://gitlab.com/lmiranda/trabalhopratico.git)

**Gitlab Pages:** [https://lmiranda.gitlab.io/trabalhopratico/](https://lmiranda.gitlab.io/trabalhopratico/)

**Heroku deployed grails app:** [https://fierce-basin-88223.herokuapp.com](https://fierce-basin-88223.herokuapp.com)




